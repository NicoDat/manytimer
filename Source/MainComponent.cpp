/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
	setSize(700, 300);

	txt_timeout.reset(new Label("txt_timeout", TRANS("Timeout : 0")));
	addAndMakeVisible(txt_timeout.get());
	txt_timeout->setEditable(false, false, false);
	txt_timeout->setColour(TextEditor::textColourId, Colours::black);
	txt_timeout->setBounds(100, 100, 100, 100);

	btn_open.reset(new TextButton("btn_open"));
	addAndMakeVisible(btn_open.get());
	btn_open->setButtonText(TRANS("CLICK ME"));
	btn_open->addListener(this);
	btn_open->setBounds(0, 0, 100, 50);

	timeout = RelativeTime(0);
	numberActiveWindow = 0;
}

MainComponent::~MainComponent()
{
	//closing everyTimers opened
	for (int i = 0; i < timerWindowList.size(); i++) {
		TimerWindow * tw = timerWindowList.getUnchecked(i);
		tw->deleteWindow();
	}
	btn_open = nullptr;
	txt_timeout = nullptr;
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
}

void MainComponent::resized()
{
   // window not resizable
}


void MainComponent::buttonClicked(Button* buttonThatWasClicked)
{
	if (buttonThatWasClicked == btn_open.get())
	{
		if (timeout == RelativeTime(0)) {
			
			lock.enterWrite();

			if (numberActiveWindow == 0) {
				for (int i = 0; i < timerWindowList.size(); i++) {
					TimerWindow * tw = timerWindowList.getUnchecked(i);
					tw->deleteWindow();
				}
				timerWindowList.clearQuick();
			}

			TimerWindow * tw = new TimerWindow();
			timerWindowList.add(tw);
			tw->setVisible(true);
			numberActiveWindow++;
			
			lock.exitWrite();

			DBG("open btn clicked : " + String(timerWindowList.size()) + " Timer(s) opened, " + String(numberActiveWindow) + " Timer(s) actived, ");
		}
		else {
			DBG("Cant open new timer if timeout is defined");
		}
	}
}

void MainComponent::setTimeout(RelativeTime RT)
{
	lock.enterWrite();

	timeout = RT;
	txt_timeout->setText("Timeout : " + String(timeout.inSeconds()),NotificationType::sendNotification);

	lock.exitWrite();

	for (int i = 0; i < timerWindowList.size(); i++) {
		TimerWindow * tw = timerWindowList.getUnchecked(i);
		tw->getMTCcontent()->getMyTimer()->setTimeout(timeout);
	}
}

void MainComponent::timerWindowClose()
{
	lock.enterWrite();

	numberActiveWindow--;
	DBG("active timerwindows : " + String(numberActiveWindow));

	if (numberActiveWindow == 0) {
		setTimeout(RelativeTime(0));
	}

	lock.exitWrite();
}

RelativeTime MainComponent::getTimeout()
{
	lock.enterRead();

	RelativeTime tmp = timeout;

	lock.exitRead();

	return tmp;
}
