/*
  ==============================================================================

    TimerWindow.h
    Created: 28 Dec 2018 5:04:13pm
    Author:  nicol

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "MyTimerComponent.h"



class TimerWindow : public DocumentWindow
{
public:
	TimerWindow();

	void closeButtonPressed() override;

	void deleteWindow();
	MyTimerComponent * getMTCcontent();

private:
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TimerWindow) 

		MyTimerComponent * MTCcontent;
	
};
