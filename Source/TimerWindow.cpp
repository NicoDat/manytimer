/*
  ==============================================================================

    TimerWindow.cpp
    Created: 3 Jan 2019 3:09:31pm
    Author:  nicol

  ==============================================================================
*/

#include "TimerWindow.h"
#include "MainComponent.h"



TimerWindow::TimerWindow() : DocumentWindow("Timer",
	Desktop::getInstance().getDefaultLookAndFeel()
	.findColour(ResizableWindow::backgroundColourId),
	DocumentWindow::closeButton)
{
	setUsingNativeTitleBar(true);
	MTCcontent = new MyTimerComponent();
	setContentOwned(MTCcontent, true);

	setSize(200, 100);
	setCentrePosition(250, 250);
	setVisible(true);
}

void TimerWindow::closeButtonPressed()
{
	// This is called when the user tries to close this window. Here, we'll just
	// ask the app to quit when this happens, but you can change this to do
	// whatever you need.
	MainComponent * MC = (MainComponent *)Desktop::getInstance().getComponent(0)->getChildComponent(0);
	MC->timerWindowClose();
	setVisible(false);
}

void TimerWindow::deleteWindow()
{
	delete this;
}

MyTimerComponent * TimerWindow::getMTCcontent()
{
	return MTCcontent;
}
