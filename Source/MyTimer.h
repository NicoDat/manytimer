#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class MyTimer
{
public:
	MyTimer();
	~MyTimer();

	void startAndStop();
	bool isStarted();
	RelativeTime getTime();
	void setTimeout(RelativeTime & relativeTime);
	RelativeTime getTimeout();

private:
	Time startTime;
	RelativeTime interval;
	RelativeTime timeout;
	bool started;

	void updateTime();

};

/*
DOCUMENTATION :

class TimerComponent    : public AnimatedAppComponent,
	public Button::Listener
{

	....

void TimerComponent::update()
{
	txt_timer->setText( String((myTimer.getTime()).inSeconds()) , NotificationType::dontSendNotification);
}

void TimerComponent::buttonClicked(Button* buttonThatWasClicked)
{
	if (buttonThatWasClicked == btn_click.get())
	{
		myTimer.startAndStop();
	}
}

*/

