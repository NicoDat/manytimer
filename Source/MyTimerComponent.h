/*
  ==============================================================================

    Animation.h
    Created: 29 Nov 2018 8:48:54pm
    Author:  nicol

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "MyTimer.h"

//==============================================================================
/*
*/
class MyTimerComponent    : public AnimatedAppComponent,
	public Button::Listener
{
public:
    MyTimerComponent();
    ~MyTimerComponent();

    void paint (Graphics&) override;
    void resized() override;
	void update() override;
	void buttonClicked(Button* buttonThatWasClicked) override;

	MyTimer * getMyTimer();

private:

	std::unique_ptr<Label> txt_timer;
	std::unique_ptr<TextButton> btn_click;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MyTimerComponent)
	MyTimer myTimer;


};
