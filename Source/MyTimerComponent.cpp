/*
  ==============================================================================

    Animation.cpp
    Created: 29 Nov 2018 8:48:54pm
    Author:  nicol

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MyTimerComponent.h"

//==============================================================================
MyTimerComponent::MyTimerComponent()
{
	// In your constructor, you should add any child components, and
	// initialise any special settings that your component needs.

	setSize(200,100);
	setFramesPerSecond(60);

	txt_timer.reset(new Label("txt_timer", TRANS("11:11")));
	addAndMakeVisible(txt_timer.get());
	txt_timer->setEditable(false, false, false);
	txt_timer->setColour(TextEditor::textColourId, Colours::black);
	txt_timer->setBounds(0, 0, getWidth() / 2, getHeight() / 2);

	btn_click.reset(new TextButton("btn_click"));
	addAndMakeVisible(btn_click.get());
	btn_click->setButtonText(TRANS("Start/Stop"));
	btn_click->addListener(this);
	btn_click->setBounds(0, getHeight() / 2, getWidth() / 2, getHeight() / 2);

}

MyTimerComponent::~MyTimerComponent()
{
	txt_timer = nullptr;
	btn_click = nullptr;
}

void MyTimerComponent::paint (Graphics& g)
{
    /* This demo code just fills the component's background and
       draws some placeholder text to get you started.

       You should replace everything in this method with your own
       drawing code..
    */

	g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));

	g.setFont(Font(16.0f));
	g.setColour(Colours::white);
	
}

void MyTimerComponent::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..

}

void MyTimerComponent::update()
{
	txt_timer->setText( String((myTimer.getTime()).inSeconds()) , NotificationType::dontSendNotification);
}

void MyTimerComponent::buttonClicked(Button* buttonThatWasClicked)
{
	if (buttonThatWasClicked == btn_click.get())
	{
		myTimer.startAndStop();
	}
}

MyTimer * MyTimerComponent::getMyTimer()
{
	return &myTimer;
}
